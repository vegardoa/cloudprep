Why it exists: Amazon had datacenters for handling website traffic, during winter they required a lot more datapower than
during summer because of amount of purchases, which left a lot of unused datapower available the rest of the year, so
they decided to sell it as a service.

Main components:
EC2               -- Elastic Cloud Computing (main):
                        Virtual servers and virtual instances with Linux, Windows or other OSes
                        On demand computer datapowerPay per use: on demand, reserved and spot pricing.
                        Popular instance types:
                            Compute optimised         CPU is very powerful.
                            Memory optimised          Large RAM storage, access and I/O
                            Storage optimised         Optimised for I/O, meaning disk reads and writes
                        Said during lecture:
                            Effectively a computer: You are renting a CPU and RAM that youre renting.
                            All the service offerings boil down to some sort of computational units which need to fuel it.
EBS               -- Elastic Block Store:
                        Can be used as a hard drive. A block device
                        Allows you to store data on a kind of "block" based file system, which is very typical to hard drives.
                        Anything that runs an operating system will require a block storage since it needs something that looks like a normal file system.
                        Primary backend for OS or database storage, because database OS-es require the storage to look certain ways and expect certain properties.
S3                -- Simple Storage Service:
                        Standard Storage:             Frequent access, Always available, Expensive.
                        Infrequent Access Storage:    Infrequent access, Less availability, Slower.
                        Glacier:                      Deep storage, Rarely used (retained for long time), Data archiving.
                        Reduced Redundancy Storage:   Non-essential, Easily reproducible data.
                        Said during lecture:
                            The way things are physically stored matters for how much it costs. Ram fast to access + read&write, but costly. On other end of spectrum: write to magnetic tape, slow but lasts long,
                            such a storage type is of type glacier, whose purpose is just to be available as back up in emergency. Once data has been written its unlikely to be read from again.
                            You need to pick which storage you want and which is appropriate since you'll be charged accordingly.
ELB               -- Elastic Load Balancing:
VPC and VPC flow  -- Virtual private cloud:
                        Flow Logging:                 native network flow analysis and logging.
                        Equivalent to netflow monitoring
                        Uses:
                            Performance.
                            Security.
                        Limitations:
                            Only primary interface logged.
                            DHCP not logged.
                            Internal AWS DNS servers traffic not logged.
                        Said during lecture:
                            Cloud service that is isolated from everything else and you have your own network to manage. Own ip address space, you manage and lay out physically how
                            it looks like, you may have european offices, US offices and so on. Kind of like a private cloud for you. Exists for two reasons: Performance and security.
                            Under public clouds your instance is shared with other users, you may not know who they are, but has in the past been data leakage or vulnerabilities which may cause
                            your data to leak to other co-hosted people, obviously some applications cannot allow that. Limitations include managing DCHP yourself, so the level of monitoring is smaller for private clouds.

Lambda            -- AKA Cloud Functions, AKA Server-less computing.
                        In App Development:                               Write and execute code without the need to understand various AWS services required to run your application and without
                                                                          dealing with complexity of auto-scaling and infrastructure Performance bottlenecks.
                        In Combination With Other Services, ex. S3:       Run a lambda function as soon as any new file gets updated to a specific S3 bucket.
                        In Combination With Events, ex. logging.          Run lambda function on specific logging events.
                        Said during lecture:
                            When youre designing your microservice or restapi, you end up with handlers which handles particular requests. You may have an application which registers multiple handlers
                            for multiple resources (URIs) and then you write the logic to handle these requests. Like a collection of different functionalities which you combine into a single
                            webapplication which you deploy. When you deployed assignment 2 it was a single monolithic system which where handling multiple things (webhooks, issues, etc..).
                            Now you can isolate individual functionality as if its a single function which take some parameter and produces some result. So for example take some registration of the webhook,
                            you have the ability to do one thing (register webhook), and get a result (error, or feedback that webhook was registered), we can isolate this single function into
                            its own kind of context, and then if we do that we can just deploy that single unit and let cloud provider manage that for us, and then we can compose our more comprehensive
                            more complex webapplication out of those single units.
                            ADVANTAGES:
                            Can isolate and move around individual pieces of functionality and upgrade for example webhook registration without touching everything else, therefore more flexible.
                            You dont need to manage a server or any form of provisioning, Sort of a buzzword to use here as a server-less computing, if youre writing a business function/logic which is deployes
                            without you caring about anything (everything else is handled for you (like heroku would)), here that exact thing is done on a very small unit (function).
                            Integration, aws platform generates a lot of events (every time there is something stored, instance comes up, goes down, every reboot etc...), if you want to react to those events
                            you can use a lambda to write a callback function which gets triggered by those events, its isolated in that its triggered by the database but does not process or interact at all directly
                            with the database itself.

Event sources:
    Changes to data in an Amazon S3 bucket.
    Changes to an Amazon DynamoDB table.
    API calls made using AWS SDKs.
    HTTP requests using API Gateway.
    Said during lecture:
        You can have all sorts of different monitoring events.
        Also events which comes from HTTP requests. HTTP is one of the event sources that can be handled by lambda function.

Lambda workflow:
    Every tiny functionality which provides a business logic to you is isolated in its own component, compiled, deployed, and managed individually.
    A lot of coordination and extra work for each functionality so you have to find a balance.

Lambda Pros:
    Charge per run-time (usage)
    Auto scaling.
    Accessible
    Easily modularise larger applications
    New business models
    Build-in monitoring and billing functions (for usage statistics).
Lambda Cons:
    Runtime rounding to 100ms
    No versioning -- improved
    Deployment workflow really needs command line automation -- watch this space
    Lock-in effect (low-level details are forced upon the app developer)
    Complicated persistence and external DB access.

--------------------------------------|----------------------|--------------------------------------
                                      |    LAMBDA LECTURE    |
--------------------------------------|----------------------|--------------------------------------

Where would you use lambdas:
    When you're developing something you want to isolate, you want multiple binaries so that you can iterate over the micro services independently.
    Every time you have some sort of event you need to deal with, it could fit in a lambda.
    Typical requests your application handles can be done i lambda. (Some disadvantages in terms of management, deployment and managing the state),
    
