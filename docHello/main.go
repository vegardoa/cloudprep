package main

import (
	"fmt"
	"net/http"
)

																						// Default answer for no text after /
func Hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello World!")
	fmt.Println("Greeting to a user")
}



func main() {
	fmt.Println("Starting service")
	http.HandleFunc("/", Hello)
	http.ListenAndServe(":8080", nil)
}
